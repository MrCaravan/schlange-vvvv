#define EPS_NOISE_FXH

#ifndef CALC_FXH
// 3D scalar field gradient
#define calcDx3D(f,p,e) ( (f(p + float3(e,0,0)) - f(p - float3(e,0,0))) / (2*e) )
#define calcDy3D(f,p,e) ( (f(p + float3(0,e,0)) - f(p - float3(0,e,0))) / (2*e) )
#define calcDz3D(f,p,e) ( (f(p + float3(0,0,e)) - f(p - float3(0,0,e))) / (2*e) )
#define calcGradS3(f,p,e) ( float3(calcDx3D(f, p, e), calcDy3D(f, p, e), calcDz3D(f, p, e)) )
#endif

float hash(float n) { return frac(sin(n)*43758.5453); }
float fastnoise(float3 x) {
// The noise function returns a value in the range -1.0f -> 1.0f
	float3 p = floor(x);
	float3 f = frac(x);
	f = f*f*(3.0-2.0*f);
    float n = p.x + p.y*57.0 + 113.0*p.z;
	return lerp(lerp(lerp( hash(n+0.0), hash(n+1.0),f.x),
                   lerp( hash(n+57.0), hash(n+58.0),f.x),f.y),
    	           lerp(lerp( hash(n+113.0), hash(n+114.0),f.x),
                   lerp( hash(n+170.0), hash(n+171.0),f.x),f.y),f.z);
}
float trignoise(float3 p) {
    float res = 0;
    float n = fastnoise(p);
    float3 t = sin(p.yzx*3.14159265 + cos(p.zxy*3.14159265+0.57))*0.5 + 0.5;
    res += (dot(t, float(0.333)) * n);
    p = p*1.5 + t;
    t = sin(p.yzx*3.14159265 + cos(p.zxy*3.14159265+1.17))*0.5 + 0.5;
    res += (dot(t, float3(0.3,-0.3,0.3))) * (1-n);    
	return saturate( res*n );
}
float3x3 m = float3x3(0,0.8,0.6, -0.8,0.36,-0.48, -0.6,-0.48,0.64);
float fbm(float3 p) {
    float f;
    f  = 0.5000*trignoise( p ); p = mul(m,p*2.02);
    f += 0.2500*trignoise( p ); p = mul(m,p*2.03);
    f += 0.1250*trignoise( p ); p = mul(m,p*2.01);
    return f;
}

/////////////// inflection ///////////////////
interface iInflectFunc {float result(float nz); };
class cNone : iInflectFunc {
	float result(float nz) {
		return nz;
	}
};
class cBillow : iInflectFunc {
	float result(float nz) {
		return abs(nz);
	}
};
class cRidge : iInflectFunc {
	float result(float nz) {
		return 1-abs(nz);
	}
};
/*
class cBias : iInflectFunc
{
	float result(float2 dist) {
	noise = sign(noise) * bias(abs(noise), FN_bias);
		return dist.y - dist.x;
	}
};
cBias Bias;
*/
cNone Noise;
cBillow Billow;
cRidge Ridge;

#ifdef NOISE_FXH

/////////////// noises ///////////////////
interface iNoiseType {float result(float3 p); };
class cPerlin : iNoiseType {
	float result(float3 p) { return perlin(p); }
};
class cSimplex : iNoiseType {
	float result(float3 p) { return simplex(p); }
};
class cWorley : iNoiseType {
	float result(float3 p) { return worley(p); }
};
class cFast : iNoiseType {
	float result(float3 p) { return fastnoise(p); }
};
cPerlin Perlin;
cSimplex Simplex;
cWorley Worley;
cFast Fast;

/////////////// more cell distances for worley ///////////////////
class cCubes : iCellDist {
	float get(float3 off) {
		return max( abs(off.x) *0.866025 + abs(off.z)*0.5 + off.y*0.5, abs(off.z) - off.y );
	}
	float get(float2 off) {
		return max( abs(off.x) *0.866025 + off.y *0.5, -off.y );
	}
};
class cEps1 : iCellDist {
	float get(float3 off) {
		return max( max( abs(off.x) + (max(abs(off.y), off.z)), -max(abs(off.y), off.z) ) ,
				        abs(off.z) - abs(off.y) ) ;
	}
	float get(float2 off) {
		return max( max( abs(off.x) + (abs(off.y)), -abs(off.y) ) , abs(off.y) ) ;
	}
};
class cEps2 : iCellDist {
	float get(float3 off) {
		return max( max( abs(off.x) + (max(off.y, off.z)), -max(off.y, off.z) ) ,
				        abs(off.z) + off.y ) ;
	}
	float get(float2 off) {
		return max( max( abs(off.x) + off.y, -off.y ) , off.y ) ;
	}
};
cCubes cubes;
cEps1 eps1;
cEps2 eps2;

#endif
